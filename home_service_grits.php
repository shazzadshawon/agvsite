<div class="isotope-item portfolio-item tall branding">
    <div class="portfolio-media">
        <a href="mobile-application.php"    class="thumb-overlay overlay-effect-1" target="">
            <img class="lazy portfolio_image_opacity" data-src="files/uploads/iconImage/Mobile_app.jpeg" alt="Mobile Application">
            <div class="overlay-caption caption-top portfolio_overlay_background portfolio_overlay-top">
                <h6 class="caption-sub portfolio-category subtitle-2">Mobile Application</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase portfolio_caption">Mobile Application</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item branding">
    <div class="portfolio-media">
        <a href="desktop-application.php" class="thumb-overlay overlay-effect-1 text-light" target="">
            <img class="portfolio_image_opacity lazy" data-src="files/uploads/iconImage/Desktop_app.jpeg" alt="Desktop Application">
            <div class="overlay-caption portfolio_overlay-bottom portfolio_overlay_background">
                <h6 class="caption-sub portfolio-category subtitle-2">Desktop Application</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase portfolio_caption">Desktop Application</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item wide photography">
    <div class="portfolio-media">
        <a href="web-application.php" class="thumb-overlay overlay-effect-1 text-light" target="">
            <img src="files/uploads/iconImage/Web_app.jpg" alt="Web Design & Application">
            <div class="overlay-caption portfolio_overlay_background" style="top: 87% !important; padding-bottom: 10%;">
                <h6 class="caption-sub portfolio-category subtitle-2">Web Design & Application</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase portfolio_caption">Web Design & Application</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item wide web">
    <div class="portfolio-media">
        <a href="system-integration.php" class="thumb-overlay overlay-effect-1 text-light" target="">
            <img src="files/uploads/iconImage/Software%20&%20hardware%20integration.jpg" alt="Mobile Application">
            <div class="overlay-caption caption-top portfolio_overlay_background" style="top: 68%;padding-bottom: 10%;">
                <h6 class="caption-sub portfolio-category subtitle-2">System Integration</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase portfolio_caption">System Integration</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item branding">
    <div class="portfolio-media">
        <a href="payment.php" class="thumb-overlay overlay-effect-1 text-light" target="">
            <img src="files/uploads/iconImage/Payment_Gateway.jpg" alt="Payment Gateway">
            <div class="overlay-caption caption-bottom portfolio_overlay_background" style="bottom: 0;">
                <h6 class="caption-sub portfolio-category subtitle-2">Payment Gateway</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase portfolio_caption">Payment Gateway</h4>
            </div>
        </a>
    </div>
</div>
<!doctype html>
<html class="no-js" lang="en-US">
<head>

    <!-- DEFAULT META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400italic,700italic" rel="stylesheet" type="text/css">

    <!-- CSS -->
    <link rel="stylesheet" id="default-style-css" href="files/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" id="fontawesome-style-css" href="files/css/font-awesome.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="ionic-icons-style-css" href="files/css/ionicons.css" type="text/css" media="all">
    <link rel="stylesheet" id="owlcarousel-css" href="files/css/owl.carousel.css" type="text/css" media="all">
    <link rel="stylesheet" id="mqueries-style-css" href="files/css/mqueries.css" type="text/css" media="all">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="files/uploads/favicon.png">

    <!-- DOCUMENT TITLE -->
    <title>Abha Maryda Banerjee | AGVCORP |</title>

</head>

<body>

<!-- PAGELOADER -->
<div id="page-loader">
    <div class="page-loader-inner">
        <span class="loader-figure"></span>
        <img class="loader-logo" src="files/uploads/logo-sudo-scroll.png" srcset="files/uploads/logo-sudo-scroll.png 1x, files/uploads/logo-sudo-scroll@2x.png 2x" alt="Loader Logo">
    </div>
</div>
<!-- PAGELOADER -->

<!-- PAGE CONTENT -->
<div id="page-content">

    <!-- HEADER -->
    <header id="header" class="header-bordered header-transparent transparent-light">
        <div class="header-inner clearfix">

            <!-- LOGO -->
            <div id="logo" class="left-float">
                <a href="index">
                   <!--  <img id="scroll-logo" src="files/uploads/logo-sudo-scroll.png" srcset="files/uploads/logo-sudo-scroll.png 1x, files/uploads/logo-sudo-scroll@2x.png 2x" alt="Logo Scroll">
                    <img id="dark-logo" src="files/uploads/logo-sudo-dark.png" srcset="files/uploads/logo-sudo-dark.png 1x, files/uploads/logo-sudo-dark@2x.png 2x" alt="Logo Dark">
                    <img id="light-logo" src="files/uploads/logo-sudo-light.png" srcset="files/uploads/logo-sudo-light.png 1x, files/uploads/logo-sudo-light@2x.png 2x" alt="Logo Light"> -->
                    <img style="" id="light-logo" src="files/uploads/agv.png" srcset="files/uploads/agv.png 2x" alt="Logo Light">
                </a>
            </div>

            <!-- MAIN NAVIGATION -->
            <div id="menu" class="right-float">
                <a href="#" class="responsive-nav-toggle"><span class="hamburger"></span></a>
                <div class="menu-inner">
                    <nav id="main-nav">
                        <ul>
                            <li class="menu-item-has-children"><a href="index">Home</a></li>
                            <li class="menu-item-has-children"><a href="mobile-application">Mobile Application</a></li>
                            <li class="menu-item-has-children"><a href="web-application">Web Design & Application</a></li>
                            <li class="menu-item-has-children"><a href="desktop-application">Desktop Application</a></li>
                            <li class="menu-item-has-children"><a href="payment">Payment Gateway</a></li>
                            <li class="menu-item-has-children"><a href="system-integration"> System Integration</a></li>
                        </ul>
                    </nav>

                    <div id="menu-misc" class="clearfix">
                        <!-- HEADER SEARCH -->
                        <div id="header-search">
                            <a href="#" id="show-search"><i class="fa fa-search"></i></a>
                            <div class="header-search-content">
                                <form action="javascript:void(0)" method="get">
                                    <a href="#" id="close-search"></a>
                                    <input type="text" name="q" class="form-control" value="" placeholder="Enter your search ...">
                                    <h5 class="subtitle-1">... & press enter to start</h5>
                                </form>
                                <div class="search-outer"></div>
                            </div>
                        </div>
                    </div><!-- END #menu-misc -->
                </div><!-- END .menu-inner -->
            </div><!-- END #menu -->

        </div> <!-- END .header-inner -->
    </header>

    <!-- HERO  -->
    <section id="hero" class="hero-auto text-light parallax-section" data-parallax-image="files/uploads/services/mobile/apps-mobile-smartphone-ss-1920.jpg">

        <div id="page-title" class="wrapper align-center">
            <h1><strong>Abha Maryda Banerjee</strong></h1>
        </div> <!-- END #page-title -->

    </section>
    <!-- HERO -->

    <!-- PAGEBODY -->
    <section id="page-body">
        <div class="wrapper">
            <div class="owl-slider nav-light">
                <div><img src="files/uploads/services/mobile/abha/Abha%20Maryda%20Banerjee1.PNG" ></div>
                <div><img src="files/uploads/services/mobile/abha/Abha%20Maryda%20Banerjee2.PNG" ></div>
                <div><img src="files/uploads/services/mobile/abha/Abha%20Maryda%20Banerjee3.PNG" ></div>
                <div><img src="files/uploads/services/mobile/abha/Abha%20Maryda%20Banerjee4.PNG" ></div>
            </div>
        </div>

        <div class="spacer-medium"></div>

        <div class="wrapper">
            <div class="column-section clearfix">
                <div class="column three-fifth">
                    <h4><strong>Description</strong></h4>
                    <p>Abha's Global Ventures</p>
                    <p>Abha Maryada Banerjee is India's first woman motivational speaker of international acclaim, rated as one of the Top Ten Life, Business and Success Coaches in Asia Pacific. An expert at Leadership, Human Peak Performance and Emotional Intelligence, Abha has been the Peak Performance/Mental Strength Coach for Indian Olympic Athletes.Having spoken at prestigious forums across Asia, she has coached and consulted top CEOs, Media personalities, Sports Stars, Business people, leaders in the making and has been closely associated with the spiritual aspect of Leadership as well. Called 'Asian Oprah' and the 'Asian Woman Motivator',she professes 'Personal Mastery' for individuals and 'Emotional Fitnessfor Corporates' for achieving human capital excellence.</p>
                    <p>Abha was recently awarded the REX KARMA VEER CHAKRA AWARD which is a Global Award for social justice.</p>
                    <p>Get Abha's global ventures on your phone -:</p>
                    <ul>
                        <li>About Abha's global ventures and the application</li>
                        <li>Works on Android OS 2.1 onwards for both Phones and Tablets </li>
                        <li>Cool Swivel Interface for Images</li>
                        <li>RSS News Feed from Google</li>
                        <li>Facebook Feeds</li>
                        <li>Twitter Feeds</li>
                        <li>YouTube Feeds</li>
                        <li>Articles Feeds </li>
                        <li>Latest updates</li>
                    </ul>
                </div>
                <div class="column two-fifth last-col">
                    <h4><strong>Details</strong></h4>
                    <p class="meta-row"><span class="meta-label">Updated</span> <span class="meta-val">April 30, 2014</span></p>
                    <p class="meta-row"><span class="meta-label">Size</span> <span class="meta-val">6.4 MB</span></p>
                    <p class="meta-row"><span class="meta-label">Installs</span> <span class="meta-val">50 - 100</span></p>
                    <p class="meta-row"><span class="meta-label">Current Version</span> <span class="meta-val">1.0</span></p>
                    <p class="meta-row"><span class="meta-label">Requires Android</span> <span class="meta-val">2.2 and up</span></p>
                    <p class="meta-row"><span class="meta-label">See it live</span> <span class="meta-val"><a href="https://play.google.com/store/apps/details?id=com.profilebaker.main.abha&hl=en"><strong>Google Play Store</strong></a></span></p>
                </div>
            </div>
        </div>

        <div class="spacer-big"></div>

        <?php include "social-media.php"; ?>

        <div class="spacer-big"></div>
        <?php include "click_to.php" ?>
    </section>
    <!-- PAGEBODY -->


    <!-- FOOTER -->
    <?php include "footer.php" ?>
    <!-- FOOTER -->

</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->

<!-- SCRIPTS -->
<script src="files/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="files/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="files/js/jquery.visible.min.js"></script>
<script type="text/javascript" src="files/js/tweenMax.js"></script>
<script type="text/javascript" src="files/js/jquery.backgroundparallax.min.js"></script>
<script type="text/javascript" src="files/js/jquery.owl.carousel.js"></script>
<script type="text/javascript" src="files/js/script.js"></script>
<!-- SCRIPTS -->

</body>
</html>
<section id="hero" class="hero-auto">

    <div class="revolution-slider-wrapper">
        <div id="revolutionslider1" class="revolution-slider" data-version="5.0">
            <ul>
                <li class="text-light" data-transition="papercut" data-masterspeed="700">
                    <!-- MAIN IMAGE -->
                    <img src="files/uploads/Slider/1_new.jpg" alt="" width="1920" height="1280">

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption sudo-caption subtitle-2" data-x="center" data-hoffset="0" data-y="center" data-voffset="['-150','-150','-140','-125']" data-fontsize="['24','24','20','18']" data-lineheight="['24','24','20','18']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:0.7" data-transform_in="o:0;y:50px;s:1000;e:Power3.easeOut;" data-transform_out="o:0" data-start="400" data-responsive_offset="on">
                        Welcome to AGV
                    </div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption" data-x="center" data-hoffset="0" data-y="center" data-voffset="['-100','-100','-95','-75']" data-width="auto" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:1000;e:Power3.easeOut;" data-transform_out="opacity:0" data-start="600">
                        <hr class="zigzag medium">
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption sudo-caption align-center" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-fontsize="['72','67','58','42']" data-lineheight="['80','75','66','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:50px;o:0;s:1000;e:Power3.easeOut;" data-transform_out="o:0" data-start="800">
                        <strong>Hope your day is <br>programed for fun</strong>
                    </div>
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption" data-x="center" data-hoffset="0" data-y="center" data-voffset="['100','100','95','75']" data-width="auto" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:50px;opacity:0;s:1000;e:Power3.easeOut;" data-transform_out="opacity:0" data-start="1000">
                        <hr class="zigzag medium">
                    </div>

                </li> <!-- END first slide -->

                <li class="text-light" data-transition="boxfade" data-slotamount="7" data-masterspeed="700">
                    <!-- MAIN IMAGE -->
                    <img src="files/uploads/Slider/stars_new.jpg" alt="" width="1680" height="1100">
                    <!-- BACKGROUND VIDEO LAYER -->
                    <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" data-videowidth="100%" data-videoheight="100%" data-videomp4="files/uploads/stars_l.mp4" data-nextslideatend="true" data-videopreload="metadata" data-videoloop="none" data-autoplay="true" data-autoplayonlyfirsttime="false"></div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption sudo-caption" data-x="center" data-hoffset="0" data-y="center" data-voffset="['-50','-50','-40','-40']" data-fontsize="['72','67','58','38']" data-lineheight="['80','75','66','46']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:50px;o:0;s:1000;e:Power3.easeOut;" data-transform_out="o:0" data-start="600">
                        <strong>We believe in Winning </strong>
                    </div>


                </li> <!-- END second slide -->
                <li class="text-light" data-transition="fade" data-masterspeed="700">
                    <!-- MAIN IMAGE -->
                    <img src="files/uploads/Slider/4.jpg" alt="" width="1920" height="1280">

                    <div class="tp-caption sudo-caption align-center" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-fontsize="['72','67','58','42']" data-lineheight="['80','75','66','50']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:50px;o:0;s:1000;e:Power3.easeOut;" data-transform_out="o:0" data-start="800">
                        <strong>We are a combo of <br> Technology & Creativity </strong>
                    </div>

                </li> <!-- END first slide -->

                <li class="text-light" data-transition="fade" data-masterspeed="700">
                    <!-- MAIN IMAGE -->
                    <img src="files/uploads/Slider/3_new.jpg" alt="" width="1680" height="1100">
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption sudo-caption align-center" data-x="center" data-hoffset="0" data-y="center" data-voffset="['20','20','10','5']" data-fontsize="['64','62','54','42']" data-lineheight="['74','72','64','52']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:50px;o:0;s:1000;e:Power3.easeOut;" data-transform_out="o:0" data-start="800">
                        <strong>Experience the comfort<br> of Working with us</strong>
                    </div>


                </li> <!-- END third slide -->
            </ul>
        </div><!-- END .revolution-slider -->
    </div> <!-- END .revolution-slider-wrapper -->

    <a href="#" id="scroll-down" class="text-light"></a>

</section>
<div class="wrapper">
    <ul class="button-group filter">
        <li></li>
        <li data-filter="*"><a href=""> show all</a></li>
        <li data-filter=".branding"><a href=""> Branding</a></li>
        <li data-filter=".web"><a href=""> Web</a></li>
        <li data-filter=".photography"><a href=""> Photography</a></li>
    </ul>
    <div id="portfolio-grid" class="isotope-grid isotope-spaced-mini portfolio-container style-modern clearfix" data-heightratio="1">

        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/seasons-wallpaper/Story%20of%20Seasons%20Wallpaper3.PNG" alt="Story of Seasons Wallpaper">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Story of Seasons Wallpaper</h4>
                    </div>
                </a>
            </div>
        </div>
        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/hijri/hijri.jpg" alt="Hijri Calendar">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Hijri Calendar</h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="isotope-item portfolio-item photography">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/chat_offline/Chat%20Offline1.PNG" alt="Chat Offline">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Chat Offline</h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="isotope-item portfolio-item web">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/wall_switcher/Wall%20Switcher1.PNG" alt="Wall Switcher">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Wall Switcher</h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="isotope-item portfolio-item photography">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/shumon/shumon.png" alt="Shumon Ahmed">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Shumon Ahmed</h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light text-light" target="">
                    <img src="files/uploads/services/mobile/4U/4U1.PNG" alt="4U">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">4U</h4>
                    </div>
                </a>
            </div>
        </div>
        <div class="isotope-item portfolio-item web">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/flashlight/Flashlight1.PNG" alt="Flash Light">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Flash Light</h4>
                    </div>
                </a>
            </div>
        </div>
        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/general-converter/General%20Converter3.PNG" alt="General Converter">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">General Converter</h4>
                    </div>
                </a>
            </div>
        </div>
        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/hijri-mini/Hijri%20Calendar%20Mini1.png" alt="Hijri Calendar Mini">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Hijri Calendar Mini</h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/shongbaad/Shongbaad1.PNG" alt="Shongbaad">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Shongbaad</h4>
                    </div>
                </a>
            </div>
        </div>
        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="system_integration_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/Xcell2SMS/Xcell2SMS1.PNG" alt="Xcell2SMS">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Xcell2SMS</h4>
                    </div>
                </a>
            </div>
        </div>

    </div> <!-- END #portfolio-grid .isotope-grid -->

</div>
<div class="column-section clearfix">
    <div class="column one-third align-right">
        <h3><strong>Our Skills</strong></h3>
        <h5 class="subtitle-2">See what we're able to do.</h5>
    </div>
    <div class="column two-third last-col">
        <div class="progress-bar-item">
            <h6 class="progress-name">Android</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #ff4848" data-perc="97"><span class="tooltip">97%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Android Web Application</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #a02727" data-perc="90"><span class="tooltip">90%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Android GO</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #ffb1b1" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">IOS</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #73b6e6" data-perc="70"><span class="tooltip">70%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">IOS Web Application</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #185a8a" data-perc="80"><span class="tooltip">80%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">IOS X-Code</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="90"><span class="tooltip">90%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">IOS Swift</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #00233c" data-perc="50"><span class="tooltip">50%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Windows</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #18898a" data-perc="50"><span class="tooltip">50%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Windows Web</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #092f2f" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">PhoneGap</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #928520" data-perc="95"><span class="tooltip">95%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">PhoneGap Web</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #464010" data-perc="96"><span class="tooltip">96%</span></div></div>
        </div>
    </div>
</div>
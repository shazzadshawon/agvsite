<!doctype html>
<html class="no-js" lang="en-US">
<head>

    <!-- DOCUMENT TITLE -->
    <title>About | AGVCORP </title>
    <?php require('head.php'); ?>

</head>

<body>

<!-- PAGELOADER -->
<?php require('page_loader.php'); ?>
<!-- PAGELOADER -->

<!-- PAGE CONTENT -->
<div id="page-content">

    <!-- HEADER -->
    <?php require('header.php'); ?>
    <!-- HEADER -->

    <!-- HERO  -->
    <section id="hero" class="hero-full  parallax-section" data-parallax-image="files/uploads/title/introduction.jpg">

        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">Welcome to AGV</h4>
            <h1><strong>We believe in Winning</strong></h1>
        </div> <!-- END #page-title -->
        <a href="#" id="scroll-down"></a>

    </section>
    <!-- HERO -->
    <!-- PAGEBODY -->
    <section id="page-body">

        <div class="wrapper">

            <div class="column-section clearfix">
                <div class="column one-third">
                    <h4>Onsite Services</h4>
                    <p>Typically, projects with local and iterative requirements derive maximum value and benefit from this type of setup. The engagement facilitates clients to get the work executed completely at their premises. The onsite model is recommended for projects that require close interaction with the client team. Asian Global Ventures provides flexibility in terms of delivery and administrative control.
                        According to this model, Asian Global Ventures engineers are deputed at the client location to supplement the client's team for predetermined durations.</p>
                    <ul>
                        <li>An onsite project manager or a client representative handles the project </li>
                        <li>This model is also appropriate for onsite consulting with the client's customers for the client's products, platforms or technologies </li>
                    </ul>
                </div>
                <div class="column one-third">
                    <h4>Offsite services</h4>
                    <p>A dedicated or on-demand offsite skilled resource pool is provided to the client. The offshore model leverages the benefits of reduced capital costs and flexible ramp up options for the client. Offsite engagement involves project initiation, requirements analysis, and execution of the project on all the stages of a software development life cycle. </p>
                    <ul>
                        <li>Asian Global Ventures executes the software engineering work for the client from Head Office delivery center, on a need-basis or as per predetermined project schedules. </li>
                        <li>A client representative would manage the project coordination on the client's side and interact with Asian Global Ventures point of contact for the project. </li>
                    </ul>
                </div>
                <div class="column one-third last-col">
                    <h4>Onsite - Offsite (Hybrid)</h4>
                    <p>The onsite-offsite is preferred for, medium to long-term engagements and for engagements that directly involve application end users. This association comprises: </p>
                    <ul>
                        <li>Onshore consultants for onsite/offsite work on temporary basis </li>
                        <li><strong>Why AGV ? :</strong>
                            Backed by a very creative & capable team, we provide simple to complex solutions for mobile, web and system integrations which require both (mobile & web) over internet along with other technologies. Solutions are not just the end of a project, the ability to implement and train the users with the new changes tagged along with the solutions is also a fundamental strength of us. Prior to developing any solutions it
                        </li>

                    </ul>
                </div>
            </div> <!-- END .column-section -->
            <div class="spacer-big"></div>
        </div> <!-- END .wrapper -->
        <!-- END .fullwidth-section -->
       <!-- <div class="wrapper">
            <div class="owl-carousel owl-spaced" data-margin="30" data-nav="false" data-dots="false" data-autoplay="true" data-items="6" data-loop="true">
                <div class="align-center"><img src="files/uploads/logo-spotify-dark.png" srcset="files/uploads/logo-spotify-dark.png 1x, files/uploads/logo-spotify-dark@2x.png 2x" alt="Logo Placeholder"></div>
                <div class="align-center"><img src="files/uploads/logo-fwa-dark.png" srcset="files/uploads/logo-fwa-dark.png 1x, files/uploads/logo-fwa-dark@2x.png 2x" alt="Logo Placeholder"></div>
                <div class="align-center"><img src="files/uploads/logo-google-dark.png" srcset="files/uploads/logo-google-dark.png 1x, files/uploads/logo-google-dark@2x.png 2x" alt="Logo Placeholder"></div>
                <div class="align-center"><img src="files/uploads/logo-wordpress-dark.png" srcset="files/uploads/logo-wordpress-dark.png 1x, files/uploads/logo-wordpress-dark@2x.png 2x" alt="Logo Placeholder"></div>
                <div class="align-center"><img src="files/uploads/logo-yt-dark.png" srcset="files/uploads/logo-yt-dark.png 1x, files/uploads/logo-yt-dark@2x.png 2x" alt="Logo Placeholder"></div>
                <div class="align-center"><img src="files/uploads/logo-sass-dark.png" srcset="files/uploads/logo-sass-dark.png 1x, files/uploads/logo-sass-dark@2x.png 2x" alt="Logo Placeholder"></div>
                <div class="align-center"><img src="files/uploads/logo-ableton-dark.png" srcset="files/uploads/logo-ableton-dark.png 1x, files/uploads/logo-ableton-dark@2x.png 2x" alt="Logo Placeholder"></div>
                <div class="align-center"><img src="files/uploads/logo-ni-dark.png" srcset="files/uploads/logo-ni-dark.png 1x, files/uploads/logo-ni-dark@2x.png 2x" alt="Logo Placeholder"></div>
            </div>
        </div> <!-- END .wrapper -->
        <?php include "click_to.php" ?>
    </section>
    <!-- PAGEBODY -->


    <!-- FOOTER -->
    <?php include "footer.php" ?>
    <!-- FOOTER -->

</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->

<?php require('foot.php'); ?>

</body>
</html>
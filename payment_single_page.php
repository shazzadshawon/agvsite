<!doctype html>
<html class="no-js" lang="en-US">
<head>

    <!-- DOCUMENT TITLE -->
    <title>Payment Gateway | AGVCORP </title>
    <?php require('head.php'); ?>

</head>

<body>

<!-- PAGELOADER -->
<?php require('page_loader.php'); ?>
<!-- PAGELOADER -->

<!-- PAGE CONTENT -->
<div id="page-content">

    <!-- HEADER -->
    <?php require('header.php'); ?>
    <!-- HEADER -->

    <!-- HERO  -->
    <section id="hero" class="hero-auto text-light parallax-section" data-parallax-image="files/uploads/title/payment.php">

        <div id="page-title" class="wrapper align-center">
            <h1>Payment Gateway System Single Page</h1>
        </div> <!-- END #page-title -->

    </section>
    <!-- HERO -->

    <!-- PAGEBODY -->
    <section id="page-body">
        <div class="wrapper">
            <div class="column-section clearfix">
                <div class="column three-fifth singleSlider">
                    <div class="owl-slider nav-light">
                        <div><img src="files/uploads/services/mobile/seasons-wallpaper/Story%20of%20Seasons%20Wallpaper1.PNG" ></div>
                        <div><img src="files/uploads/services/mobile/seasons-wallpaper/Story%20of%20Seasons%20Wallpaper2.PNG" ></div>
                        <div><img src="files/uploads/services/mobile/seasons-wallpaper/Story%20of%20Seasons%20Wallpaper3.PNG" ></div>
                        <div><img src="files/uploads/services/mobile/seasons-wallpaper/Story%20of%20Seasons%20Wallpaper4.PNG" ></div>
                    </div>
                </div>
                <div class="column two-fifth last-col">
                    <h4><strong>Details</strong></h4>
                    <p class="meta-row"><span class="meta-label">Updated</span> <span class="meta-val">June 20, 2012</span></p>
                    <p class="meta-row"><span class="meta-label">Size</span> <span class="meta-val">13 MB</span></p>
                    <p class="meta-row"><span class="meta-label">Installs</span> <span class="meta-val">100 - 500</span></p>
                    <p class="meta-row"><span class="meta-label">Current Version</span> <span class="meta-val">1.0</span></p>
                    <p class="meta-row"><span class="meta-label">See it live</span> <span class="meta-val"><a href="https://play.google.com/store/apps/details?id=biz.agvcorp.storyofseasons&hl=en"><strong>payment gateway system Link</strong></a></span></p>
                </div>
            </div>
        </div>

        <div class="spacer-medium"></div>

        <div class="wrapper">
            <div class="column-section clearfix">

                <div class="panel with-nav-tabs panel-info">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1info" data-toggle="tab"><h5>Description</h5></a></li>
                            <li><a href="#tab2info" data-toggle="tab"> <h5> Features</h5></a></li>
                            <li><a href="#tab3info" data-toggle="tab"> <h5>Reviews</h5></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1info">
                                <p>
                                    Bringing you the story of seasons featuring summer, winter, autumn & rain. Each image concept is original to its single pixel. The images are developed 100% in-house with our own graphics and design team. Ideas and concepts came from all of our fans and team members.
                                </p>
                                <p>
                                    We hope you like it as much as we loved to make it.
                                </p>
                                <p>
                                    We are coming up regularly with more apps and require your support and constructive comments. Keep checking back for more apps and give us lots of stars and reviews!!
                                </p>
                                <p>
                                    AGV with your support is continuously "bringing imaginations into reality" through application development and solutions
                                </p>
                                <p>
                                    NOTE: The app is only for 10.1 Tablets
                                </p>
                            </div>
                            <div class="tab-pane fade" id="tab2info">
                                <p>
                                    This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project
                                </p>
                                <p>
                                    We hope you like it as much as we loved to make it.
                                </p>
                                <p>
                                    This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project
                                </p>
                                <p>
                                    This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project
                                </p>
                                <p>
                                    This is the Feature of this Project This is the Feature of this Project This is the Feature of this Project
                                </p>
                            </div>
                            <div class="tab-pane fade" id="tab3info">
                                <p>
                                    Review One
                                </p>
                                <p>
                                    Review Two
                                </p>
                                <p>
                                    Review Three
                                </p>
                                <p>
                                    Review Four
                                </p>
                                <p>
                                    Review Five
                                </p>
                            </div>
                            <div class="tab-pane fade" id="tab4info">Info 4</div>
                            <div class="tab-pane fade" id="tab5info">Info 5</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="spacer-big"></div>
        <div class="spacer-big"></div>
        <?php include "click_to.php" ?>
    </section>
    <!-- PAGEBODY -->


    <!-- FOOTER -->
    <?php include "footer.php" ?>
    <!-- FOOTER -->

</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->

<!-- SCRIPTS -->
<?php require('foot.php'); ?>
<!-- SCRIPTS -->

</body>
</html>
<div class="fullwidth-section text-light parallax-section lazy" data-parallax-image="files/uploads/title/msg.png">
    <div class="fullwidth-content wrapper-small align-center">

        <div class="owl-slider content-slider nav-light" data-dots="true">
            <div>
                <blockquote>Are you in a hurry? Or coding is just too overwhelming for you? Let me customize your app for you.<br>
                    It's not just about customization; it's about you and me together building a better experience for your users.
                    <cite>We offer CUSTOM services</cite>
                </blockquote>
            </div>
            <div>
                <blockquote>We make the web a better place for you and us because designing a website is designing a relationship for us.
                    <cite>We are SMALL enough to care & BIG enough to conquer</cite>
                </blockquote>
            </div>
        </div>

    </div>
</div> <!-- END .fullwidth-section -->
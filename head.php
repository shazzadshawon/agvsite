<?php error_reporting(0); ?>

<!-- DEFAULT META TAGS -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- FONTS -->
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" async defer>
<link href="http://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lora:400italic,700italic" rel="stylesheet" type="text/css">

<!-- CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.0/css/ionicons.css" />

<!--<link rel="stylesheet" id="default-style-css" href="files/css/style.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="ionic-icons-style-css" href="files/css/ionicons.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="revolution-slider-main-css" href="files/revolution/css/settings.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="revolution-slider-layer-css" href="files/revolution/css/layers.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="revolution-slider-nav-css" href="files/revolution/css/navigation.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="owlcarousel-css" href="files/css/owl.carousel.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="lightcase-css" href="files/css/lightcase.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="isotope-style-css" href="files/css/isotope.css" type="text/css" media="all">-->
<!--<link rel="stylesheet" id="mqueries-style-css" href="files/css/mqueries.css" type="text/css" media="all">-->
<link rel="stylesheet" id="mqueries-style-css" href="files/css/allStyles.min.css" type="text/css" media="all">
<!--<link rel="stylesheet" id="mqueries-style-css" href="files/revolution/css/revo.min.css" type="text/css" media="all">-->

<!-- FAVICON -->
<link rel="shortcut icon" href="files/uploads/favicon.ico">


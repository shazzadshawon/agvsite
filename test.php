<div class="column-section boxed-spaced adapt-height clearfix">
    <div class="column one-half">
        <h5>CREATIVE & TECHNICAL</h5>
        <p>Whether it's website or graphic design, system development or custom programming we like to keep everything under one roof to make it easier for our customers.</p>
    </div>
    <div class="column one-half">
        <h5>PASSIONATE</h5>
        <p>We love nothing more that working on a great project with a fantastic client. We do care about our clients and can often be found working out of hours to get everything 'just right'!</p>
    </div>
</div>
<div class="column-section boxed-spaced adapt-height clearfix">
    <div class="column one-half">
        <h5>WE PAY ATTENTION</h5>
        <p>We Observe, we argue, we listen, we discuss, we guide. We line to your ideas, plans, Understand your requirement and objectives for your business. We then analysis market, your position and future for your product. If we feel we're not a good fit we'll be honest and tell you from the outset.</p>
    </div>
    <div class="column one-half">
        <h5>INVOLVEMENT</h5>
        <p>As a company we have been working since 2006 however we�ve been developing software�s, Mobile application and websites since then. We love discussing and planning new projects and have years of knowledge and experience that we bring to the table.</p>
    </div>
</div>
﻿<!doctype html>
<html class="no-js" lang="en-US">
    <head>

        <!-- DOCUMENT TITLE -->
        <title>Home | AGVCORP </title>
        <?php require('head.php'); ?>

    </head>

    <body>

    <!-- PAGELOADER -->
<!--    --><?php //require('page_loader.php'); ?>
    <!-- PAGELOADER -->

    <!-- PAGE CONTENT -->
    <div id="page-content">

        <!-- HEADER -->
        <?php require('header.php'); ?>
        <!-- HEADER -->

        <!-- HERO  -->
        <?php require('home_slider.php'); ?>
        <!-- HERO -->

        <!-- PAGEBODY -->
        <section id="page-body">

            <!--  SERVICE-->
            <?php require('home_service.php'); ?>
            <!--  //SERVICE-->

            <div class="spacer-big"></div>

            <!--  Message Section-->
            <?php require('home_message.php'); ?>
            <!--  //Message Section-->

            <div class="spacer-big"></div>

            <!--  Descriptio Section-->
            <?php require('home_description.php'); ?>
            <!--  //Descriptio Section-->

            <div class="spacer-big"></div>

            <!--  Numbers-->
            <?php require('home_numbers.php'); ?>
            <!--  //Numbers-->

             <!-- END .column-section -->
        <?php include "click_to.php" ?>
        </section>
        <!-- PAGEBODY -->


        <!-- FOOTER -->
        <?php include "footer.php" ?>
        <!-- FOOTER -->

    </div> <!-- END #page-content -->
    <!-- PAGE CONTENT -->

    <?php require('foot.php'); ?>

    </body>
</html>
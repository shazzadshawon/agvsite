<div class="column-section boxed-sticky adapt-height vertical-center text-light align-center clearfix">
    <div class="column one-third bigpadding" style="background:url(files/uploads/index-main-1.jpg) center center;background-size:cover;">
        <h5 class="subtitle-2">More than</h5>
        <div class="sr-counter counter-big"><span class="counter-value">947</span></div>
        <h5 class="uppercase">Projects delivered</h5>
    </div>
    <div class="column one-third bigpadding" style="background:url(files/uploads/index-main-2.jpg) center center;background-size:cover;">
        <h5 class="subtitle-2">A list of</h5>
        <div class="sr-counter counter-big"><span class="counter-value">367</span></div>
        <h5 class="uppercase">Satisfied Clients</h5>
    </div>
    <div class="column one-third last-col bigpadding" style="background:url(files/uploads/index-main-3.jpg) center center;background-size:cover;">
        <h5 class="subtitle-2">We wrote exactly</h5>
        <div class="sr-counter counter-big"><span class="counter-value">91 429</span></div>
        <h5 class="uppercase">Lines of code</h5>
    </div>
</div>
<div class="isotope-item portfolio-item wide branding">
    <div class="portfolio-media">
        <a href="mobile-application.php" class="thumb-overlay overlay-effect-1">
            <img src="files/uploads/iconImage/Mobile_app.jpeg" alt="Mobile Application">
            <div class="overlay-caption caption-top">
                <h6 class="caption-sub portfolio-category subtitle-2">Mobile Application</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase">Mobile Application</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item wide branding">
    <div class="portfolio-media">
        <a href="web-application.php" class="thumb-overlay overlay-effect-1 text-light">
            <img src="files/uploads/iconImage/Web_app.jpg" alt="Web Design & Application">
            <div class="overlay-caption">
                <h6 class="caption-sub portfolio-category subtitle-2">Web Design & Application</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase">Web Design & Application</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item  wide photography">
    <div class="portfolio-media">
        <a href="desktop-application.php" class="thumb-overlay overlay-effect-1 text-light">
            <img src="files/uploads/iconImage/Desktop_app.jpeg" alt="Desktop Application">
            <div class="overlay-caption">
                <h6 class="caption-sub portfolio-category subtitle-2">Desktop Application</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase">Desktop Application</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item wide tall web">
    <div class="portfolio-media">
        <a href="payment.php" class="thumb-overlay overlay-effect-1 text-light">
            <img src="files/uploads/iconImage/Payment_Gateway.jpg" alt="Payment Gateway">
            <div class="overlay-caption caption-bottom">
                <h6 class="caption-sub portfolio-category subtitle-2">Payment Gateway</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase">Payment Gateway</h4>
            </div>
        </a>
    </div>
</div>
<div class="isotope-item portfolio-item wide  branding">
    <div class="portfolio-media">
        <a href="system-integration.php" class="thumb-overlay overlay-effect-1 text-light">
            <img src="files/uploads/iconImage/Software%20&%20hardware%20integration.jpg" alt="Mobile Application">
            <div class="overlay-caption caption-top">
                <h6 class="caption-sub portfolio-category subtitle-2">System Integration</h6>
                <hr class="zigzag">
                <h4 class="caption-name portfolio-name uppercase">System Integration</h4>
            </div>
        </a>
    </div>
</div>
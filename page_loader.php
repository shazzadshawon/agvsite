<div id="page-loader">
    <div class="page-loader-inner">
        <span class="loader-figure"></span>
        <img class="loader-logo" src="files/uploads/loader_image_text.png" srcset="files/uploads/loader_image_text.png 1x, files/uploads/loader_image_text.png 2x" alt="Loader Logo">
    </div>
</div>
<!doctype html>
<html class="no-js" lang="en-US">
<head>

    <!-- DEFAULT META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- FONTS -->
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lora:400italic,700italic" rel="stylesheet" type="text/css">

    <!-- CSS -->
    <link rel="stylesheet" id="default-style-css" href="files/css/style.css" type="text/css" media="all">
    <link rel="stylesheet" id="fontawesome-style-css" href="files/css/font-awesome.min.css" type="text/css" media="all">
    <link rel="stylesheet" id="ionic-icons-style-css" href="files/css/ionicons.css" type="text/css" media="all">
    <link rel="stylesheet" id="mqueries-style-css" href="files/css/mqueries.css" type="text/css" media="all">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="files/uploads/favicon.png">

    <!-- DOCUMENT TITLE -->
    <title>Start your project with us | Sudo | High Performance Template</title>

</head>

<body>

<!-- PAGELOADER -->
<div id="page-loader">
    <div class="page-loader-inner">
        <span class="loader-figure"></span>
        <img class="loader-logo" src="files/uploads/logo-sudo-scroll.png" srcset="files/uploads/logo-sudo-scroll.png 1x, files/uploads/logo-sudo-scroll@2x.png 2x" alt="Loader Logo">
    </div>
</div>
<!-- PAGELOADER -->

<!-- PAGE CONTENT -->
<div id="page-content">

    <!-- HEADER -->
    <header id="header" class="header-bordered header-transparent transparent-light">
        <div class="header-inner clearfix">

            <!-- LOGO -->
            <div id="logo" class="left-float">
                <a href="index">
                    <img id="scroll-logo" src="files/uploads/logo-sudo-scroll.png" srcset="files/uploads/logo-sudo-scroll.png 1x, files/uploads/logo-sudo-scroll@2x.png 2x" alt="Logo Scroll">
                    <img id="dark-logo" src="files/uploads/logo-sudo-dark.png" srcset="files/uploads/logo-sudo-dark.png 1x, files/uploads/logo-sudo-dark@2x.png 2x" alt="Logo Dark">
                    <img id="light-logo" src="files/uploads/logo-sudo-light.png" srcset="files/uploads/logo-sudo-light.png 1x, files/uploads/logo-sudo-light@2x.png 2x" alt="Logo Light">
                </a>
            </div>

            <!-- MAIN NAVIGATION -->
            <div id="menu" class="right-float">
                <a href="#" class="responsive-nav-toggle"><span class="hamburger"></span></a>
                <div class="menu-inner">
                    <nav id="main-nav">
                        <ul>
                            <li class="menu-item-has-children"><a href="index">Home</a></li>
                            <li class="menu-item-has-children"><a href="introduction">Introduction</a></li>
                            <li class="menu-item-has-children"><a href="services">What We Do</a></li>
                            <li class="menu-item-has-children"><a href="contact-us">How to Reach Us</a></li>
                        </ul>
                    </nav>

                    <div id="menu-misc" class="clearfix">

                        <!-- HEADER SEARCH -->
                        <div id="header-search">
                            <a href="#" id="show-search"><i class="fa fa-search"></i></a>
                            <div class="header-search-content">
                                <form action="javascirpt:void(0);" method="get">
                                    <a href="#" id="close-search"></a>
                                    <input type="text" name="q" class="form-control" value="" placeholder="Enter your search ...">
                                    <h5 class="subtitle-1">... & press enter to start</h5>
                                </form>
                                <div class="search-outer"></div>
                            </div>
                        </div>
                    </div><!-- END #menu-misc -->
                </div><!-- END .menu-inner -->
            </div><!-- END #menu -->

        </div> <!-- END .header-inner -->
    </header>

    <!-- HERO  -->
    <section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="files/uploads/title/with.jpg">
        <div id="page-title" class="wrapper align-center">

        </div> <!-- END #page-title -->
    </section>
    <!-- HERO -->


    <!-- PAGEBODY -->
    <section id="page-body">

        <div class="wrapper">

            <div id="blog-single" class="main-content left-float">

                <div class="post-content">
                    <h5 class="uppercase align-center">THE RIGHT TEAM FOR YOUR PROJECT</h5>
                    <p style="color: #000000">
                        We don't offer a typical set of programs. We don't do the same boring plot day-after-day. Everything we do is customized for our clients. We are an on stop IT solution provider and we provide everything to successfully market your business to customer.<br><br>
                        We work with you to find out what you need. We talk about your instant needs and your long-term goals and objectives. We figure out where you are and where you need to go.<br><br>
                        <img src="files/uploads/title/Team%20work.png"><br>
                        Sometimes it's simple and sometimes it's more complicated but we always give you the best solution for your project at honest price. We help you work smarter. We guide you produce more revenue. We train your staff to deal with the technical maintenance. We design strategy for the future. We always keep you informed of day-to-day developments of your project.<br><br>
                        Our services include planning and strategy, building web applications and custom programming, website design and development, System integration, payment gateway and a range of other complimentary digital marketing services.
                        <br><br><img src="files/uploads/title/Why%20choose%20us.PNG">
                    </p>
                </div> <!-- END .post-content -->
                <div class="column-section boxed-spaced adapt-height clearfix">
                    <div class="column one-half">
                        <h5>WE PAY ATTENTION</h5>
                        <p>We Observe, we argue, we listen, we discuss, we guide. We line to your ideas, plans, Understand your requirement and objectives for your business. We then analysis market, your position and future for your product. If we feel we're not a good fit we'll be honest and tell you from the outset.</p>
                    </div>
                    <div class="column one-half last-col">
                        <h5>INVOLVEMENT</h5>
                        <p>As a company we have been working since 2008 however we've been developing software's, Mobile application and websites since then. We love discussing and planning new projects and have years of knowledge and experience that we bring to the table.</p>
                    </div>
                </div>
                <div class="column-section boxed-spaced adapt-height clearfix">
                    <div class="column one-half">
                        <h5>CREATIVE & TECHNICAL</h5>
                        <p>Whether it's website or graphic design, system development or custom programming we like to keep everything under one roof to make it easier for our customers.</p>
                    </div>
                    <div class="column one-half last-col">
                        <h5>PASSIONATE</h5>
                        <p>We love nothing more that working on a great project with a fantastic client. We do care about our clients and can often be found working out of hours to get everything 'just right'!</p>
                    </div>
                </div>
                <?php include "social-media.php" ?>
            </div> <!-- END #blog-single .main-content -->

            <aside class="sidebar right-float">
                <div class="sidebar-content">
                    <div class="widget widget_tag_cloud">
                        <h6 class="widget-title uppercase">Start from here</h6>
                        <form id="contact-form" class="checkform sendemail">
                            <div class="form-row">
                                <label for="name">Name <abbr title="required" class="required">*</abbr></label>
                                <input type="text" name="name" id="name" class="name req" value="">
                            </div>
                            <div class="form-row">
                                <label for="email">Email <abbr title="required" class="required">*</abbr></label>
                                <input type="text" name="email" id="email" class="email req" value="">
                            </div>

                            <div class="form-row">
                                <label for="message">Project Summary <abbr title="required" class="required">*</abbr></label>
                                <textarea name="message" id="message" class="message req" rows="15" cols="50"></textarea>
                            </div>

                            <div class="form-row form-note">
                                <div class="alert-error">
                                    <h5>Something went wrong</h5>
                                    Please check your entries!
                                </div>
                            </div>

                            <div class="form-row hidden">
                                <input type="text" id="form-check" name="form-check" value="" class="form-check">
                            </div> <!-- Spam check field -->

                            <div class="form-row">
                                <input type="submit" name="submit" class="submit" value="Send Message">
                            </div>

                            <input type="hidden" name="subject" value="Contact Subject Sudo html">
                            <input type="hidden" name="fields" value="name,email,message">
                        </form>
                    </div>
                </div>
            </aside> <!-- END .sidebar -->
            <div class="clear"></div>

        </div> <!-- END .wrapper -->
        <div class="spacer-medium"></div>
    </section>
    <!-- PAGEBODY -->


    <!-- FOOTER -->
    <?php include "footer.php" ?>
    <!-- FOOTER -->

</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->

<!-- SCRIPTS -->
<script src="files/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="files/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="files/js/jquery.visible.min.js"></script>
<script type="text/javascript" src="files/js/tweenMax.js"></script>
<script type="text/javascript" src="files/js/jquery.backgroundparallax.min.js"></script>
<script type="text/javascript" src="files/js/script.js"></script>
<!-- SCRIPTS -->

</body>
</html>
<header id="header" class="header-bordered header-transparent transparent-light">
    <div class="header-inner clearfix">

        <!-- LOGO -->
        <div id="logo" class="left-float">
            <a href="index.php">
                <!--                    <img id="scroll-logo" src="files/uploads/logo-custom-scroll.jpg" srcset="files/uploads/logo-custom-scroll.jpg 1x, files/uploads/logo-custom-scroll@2x.jpg 2x" alt="Logo Scroll">-->
                <!--                    <img id="dark-logo" src="files/uploads/logo-custom-scroll.jpg" srcset="files/uploads/logo-custom-scroll.jpg 1x, files/uploads/logo-custom-scroll@2x.jgp 2x" alt="Logo Dark">-->
                <img style="" id="light-logo" src="files/uploads/logo_style_2.png" srcset="files/uploads/logo_style_2.png 1x, files/uploads/logo_style_2@2x.png 2x" alt="AVG LOGO">
            </a>
        </div>

        <!-- MAIN NAVIGATION -->
        <div id="menu" class="right-float clearfix">
            <a href="#" class="responsive-nav-toggle"><span class="hamburger"></span></a>
            <div class="menu-inner">
                <nav id="main-nav">
                    <ul>
                        <li class="menu-item-has-children"><a href="index.php">Home</a></li>
                        <li class="menu-item-has-children"><a href="introduction.php">Introduction</a></li>
<!--                        <li class="menu-item-has-children"><a href="services">What We Do</a></li>-->
                        <li class="menu-item-has-children"><a href="contact-us.php">How to Reach Us</a></li>
                        <li class="menu-item-has-children"><a href="services.php">What We Do</a>
                            <ul class="submenu">
                                <li class="menu-item-has-children"><a href="mobile-application.php">Mobile Application</a></li>
                                <li class="menu-item-has-children"><a href="web-application.php">Web Design & Application</a></li>
                                <li class="menu-item-has-children"><a href="desktop-application.php">Desktop Application</a></li>
                                <li class="menu-item-has-children"><a href="payment.php">Payment Gateway</a></li>
                                <li class="menu-item-has-children"><a href="system-integration.php"> System Integration</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>

                <div id="menu-misc" class="clearfix">

                    <!-- HEADER SEARCH -->
                    <div id="header-search">
                        <a href="#" id="show-search"><i class="fa fa-search"></i></a>
                        <div class="header-search-content">
                            <form action="javascript:void(0);" method="get">
                                <a href="#" id="close-search"></a>
                                <input type="text" name="q" class="form-control" value="" placeholder="Enter your search ...">
                                <h5 class="subtitle-1">... & press enter to start</h5>
                            </form>
                            <div class="search-outer"></div>
                        </div>
                    </div>
                </div><!-- END #menu-misc -->
            </div><!-- END .menu-inner -->
        </div><!-- END #menu -->

    </div> <!-- END .header-inner -->
</header>

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11&appId=1178177375648833';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
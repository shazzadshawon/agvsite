<div class="align-center">
    <h2><strong>User Experience</strong></h2>
    <h5 class="subtitle-2">We Keep all Reviews from all of our clients as such valuable Tokens</h5>
    <div class="spacer-medium"></div>
    <hr class="small thick">
    <div class="fullwidth-section text-light parallax-section" data-parallax-image="files/uploads/title/reviewbanner.png">
        <div class="fullwidth-content wrapper-small align-center">
            <div class="owl-slider content-slider nav-light" data-dots="true" >

                <div>
                    <blockquote> Masha Allah...it's great Jazakallahu khair. I just want to know how we can zoom the Date in order to see it clear specially in S4 Mini.
                        <cite><strong>Maj Hawaan</strong></cite>
                    </blockquote>
                </div>

                <div>
                    <blockquote>Excellent software This application is very helpful. To me, it's a complete year calendar with all the important Islamic events outlined vividly. May Allah bless the lives, knowledge and wisdom of the proponents of this educative and problem solving software.
                        <cite><strong>Mohammed Aminul Adam</strong></cite>
                    </blockquote>
                </div>

                <div>
                    <blockquote>Good App. Alhamdulillah that there are apps like this to help the brothers & sisters with keeping track of these Holy months. Salaamu 'Alaikum
                        <cite><strong>Ilyas Muhsin</strong></cite>
                    </blockquote>
                </div>
                <div>
                    <blockquote>Nice... good information! I find it hard to adjust the date so that its accurately match with the Gregorian date. And it would be nice if you could add day description on the calendar. And one last thing, :-) ...could you give me an option to hide or remove the intro splash screen every time I run the app. It's kind of lame seeing that screen every time I want to check on the date. Overall this is one great and superb app. JazakAllah.
                        <cite><strong>Ahmed reaz</strong></cite>
                    </blockquote>
                </div>

                <div>
                    <blockquote>Thank you Nice app, downloaded a moon phase app to keep track of the phases but this like 2 in one!
                        <cite><strong>Atiya Karim</strong></cite>
                    </blockquote>
                </div>
                <div>
                    <blockquote>Nice wallpaper app Wallpapers are so nice. All wallpapers represent four seasons.
                        <cite><strong>Viva ahmed  </strong></cite>
                    </blockquote>
                </div>

            </div>

        </div>
    </div> <!-- END .fullwidth-section -->
</div>
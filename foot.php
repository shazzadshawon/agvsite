<!-- SCRIPTS -->
<script src="files/js/jquery-2.1.4.min.js"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.min.js" async defer></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ntegrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous" async defer></script>

<!--Lazy Loader-->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.6/jquery.lazy.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.6/jquery.lazy.plugins.min.js"></script>

<!--<script type="text/javascript" src="files/js/jquery.easing.1.3.js"></script>-->
<!--<script type="text/javascript" src="files/js/jquery.visible.min.js"></script>-->
<script type="text/javascript" src="files/revolution/js/jquery.themepunch.tools.min.js?rev=5.0" async defer></script>
<script type="text/javascript" src="files/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0" async defer></script>
<script type="text/javascript" src="files/revolution/js/extensions/revolution.extension.slideanims.min.js" async defer></script>
<script type="text/javascript" src="files/revolution/js/extensions/revolution.extension.layeranimation.min.js" async defer></script>
<script type="text/javascript" src="files/revolution/js/extensions/revolution.extension.navigation.min.js" async defer></script>
<script type="text/javascript" src="files/revolution/js/extensions/revolution.extension.video.min.js" async defer></script>
<script type="text/javascript" src="files/revolution/js/extensions/revolution.extension.actions.min.js" async defer></script>
<!--<script type="text/javascript" src="files/js/tweenMax.js"></script>-->
<!--<script type="text/javascript" src="files/js/jquery.backgroundparallax.min.js"></script>-->
<!--<script type="text/javascript" src="files/js/jquery.isotope.min.js"></script>-->
<!--<script type="text/javascript" src="files/js/packery.min.js"></script>-->
<!--<script type="text/javascript" src="files/js/jquery.imagesloaded.min.js"></script>-->
<!--<script type="text/javascript" src="files/js/jquery.owl.carousel.js"></script>-->
<!--<script type="text/javascript" src="files/js/jquery.lightcase.min.js"></script>-->

<!--<script type="text/javascript" src="files/js/revolution.min.js" ></script>-->
<script type="text/javascript" src="files/js/agv.js" async></script>
<script type="text/javascript" src="files/js/customscript.min.js" async></script>
<!-- SCRIPTS -->
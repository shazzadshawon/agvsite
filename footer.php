<footer id="footer" class="footer-dark text-light">
    <div class="footer-inner wrapper">

        <div class="column-section clearfix">
            <div class="column dher-fifth">
                <div class="widget">
<!--                    <img style="width: 250px" src="files/uploads/agvLogo_new.png" srcset="files/uploads/agvLogo_new.png 1x, files/uploads/agvLogo_new@2x.png 2x" alt="Footer Logo">-->
                    <img style="width: 280px; height: 115px;" src="files/uploads/agvLogo_new.png" srcset="files/uploads/agvLogo_new.png 1x, files/uploads/agvLogo_new@2x.png 2x" alt="Footer Logo">
                    <div class="spacer-mini"></div>
                    <p>We are a combo of technology & creativity . <br>We provide perfect and professional services,  covering all areas  of the modern technology.</p>
                </div>
                <div class="widget">
                    <ul class="socialmedia-widget hover-slide-1">
                        <li class="facebook size-huge"><a href="https://www.facebook.com/agvcorp/?fref=ts)" target="_blank"></a></li>
                        <li class="twitter size-huge"><a href="https://twitter.com/agvcorp" target="_blank"></a></li>
                        <li class="linkedin size-huge"><a href="https://www.linkedin.com/company/asian-global-ventures" target="_blank"></a></li>
                        <li class="googleplus size-huge"><a href="https://plus.google.com/112250042386612154392/about" target="_blank"></a></li>
                        <li class="youtube size-huge"><a href="javascript:void (0)"></a></li>
                        <li class="pinterest size-huge"><a href="javascript:void (0)"></a></li>
                    </ul>
                </div>
            </div>
            <div class="column customone-fifth">
                <div class="widget">
                    <h6 class="widget-title uppercase">Quick Menu</h6>
                    <ul class="menu">
                        <li><a href="with-us.php">About Us</a></li>
                        <li><a href="services.php">Portfolio</a></li>
                        <li><a href="with-us.php#team">Our Team</a></li>
                        <li><a href="javascript:void (0)">Support</a></li>
                        <li><a href="contact-us.php">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="column customone-fifth">
                <div class="widget">
                    <h6 class="widget-title uppercase">Categories</h6>
                    <ul class="menu">
                        <li><a href="mobile-application.php">Mobile Applications</a></li>
                        <li><a href="web-application.php">Web Applications</a></li>
                        <li><a href="desktop-application.php">Desktop Applications</a></li>
                        <li><a href="payment.php">Payment Gateway</a></li>
                        <li><a href="system-integration.php">System Integration</a></li>
                    </ul>
                </div>
            </div>
            <div class="column two-fifth last-col">
                <div class="widget widget_recent_entries">
                    <h6 class="widget-title uppercase">Let's Be Social</h6>
                    <div class="spacer-mini"></div>
                    <div class="fb-page" data-href="https://www.facebook.com/agvcorp/?fref=ts)" data-height="200" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/agvbd/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/agvbd/">Asian Global Ventures -BD Company Limited.</a></blockquote></div>
                    <ul>
                        <li><a href="javascript:void (0)"></a><span class="post-date">Like &amp; Share us on Facebook</span></li>
                        <li><a href="javascript:void (0)">
                                <a href="https://twitter.com/agvthailand?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-size="small" data-show-count="true" data-show-screen-name="false">Follow</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                <div class="g-plus" data-action="share" data-height="20" data-href="https://plus.google.com/106105428823633446103"></div>
                                <script type="IN/FollowCompany" data-id="2414387" data-counter="right"></script>
                                <script src="https://platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
                            </a><span class="post-date"></span></li>
                        <li><a href="javascript:void (0)">
<!--                                <a href="https://www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" data-pin-count="beside" data-pin-tall="20" style="width: 100px"></a>-->
                                <a data-pin-count="beside" data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/" data-pin-custom="true"><img src="files/uploads/icon_64x64.png" width="25" height="25"></a>
                            </a><span class="post-date"></span></li>
                    </ul>
                </div>
            </div>
        </div>
        <a id="backtotop" href="#">Back to top</a>
    </div>
    <div class="copyright"><small>Copyright &copy; 2016 - 2018 by AGVCORP - Made with Love by <a href="http://www.agvcorp.biz">Asain Global Ventures (BD) Ltd.</a></small></div>
</footer>

<script type="text/javascript">
    (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/platform.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();
</script>

<script
        type="text/javascript"
        async defer
        src="//assets.pinterest.com/js/pinit.js"
></script>
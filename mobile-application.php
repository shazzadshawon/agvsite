<!doctype html>
<html class="no-js" lang="en-US">
<head>

    <!-- DOCUMENT TITLE -->
    <title>Mobile Applications | AGVCORP </title>
    <?php require('head.php'); ?>

</head>

<body>

<!-- PAGELOADER -->
<?php require('page_loader.php'); ?>
<!-- PAGELOADER -->

<!-- PAGE CONTENT -->
<div id="page-content">

    <!-- HEADER -->
    <?php require('header.php'); ?>
    <!-- HEADER -->

    <!-- HERO  -->
    <section id="hero" class="hero-big parallax-section text-light" data-parallax-image="files/uploads/title/mobile_2.jpg">
        <div id="page-title" class="wrapper align-center">
            <h1><strong>Mobile Application</strong></h1>
        </div> <!-- END #page-title -->
    </section>
    <!-- HERO -->

    <!-- PAGEBODY -->
    <section id="page-body">

        <!--  Mobile Skills-->
        <?php require('mobile_skills.php'); ?>
        <!--  //Mobile Skills-->

        <div class="spacer-small"></div>
        <div class="wrapper-small align-center">
            <h3><strong>Our R&amp;D Lab</strong></h3>
            <h5 class="subtitle-2">Our Currently Running Projects and Upcomming projects</h5>
        </div>
        <!--  Lab Grits-->
        <div class="spacer-big"></div>
        <?php require('rnd_lab.php'); ?>
        <!--  //Lab Grits-->

        <div class="spacer-big"></div>
        <div class="wrapper-small align-center">
            <h2><strong>Our Projects</strong></h2>
            <h5 class="subtitle-2">All our completed and currently in service as Mobile Apps &amp; Services</h5>
        </div>
        <!--  Project Grits-->
        <div class="spacer-big"></div>
        <?php require('mobile_projects.php'); ?>
        <!--  //Project Grits-->

        <div class="spacer-big"></div>
        <?php require('category_review.php'); ?>

<!--        --><?php //require "social-media.php"; ?>
<!--        <div class="spacer-big"></div>-->

        <?php  require "click_to.php"?>
    </section>
    <!-- PAGEBODY -->


    <!-- FOOTER -->
    <?php include "footer.php" ?>
    <!-- FOOTER -->

</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->

<!-- SCRIPTS -->
<?php require('foot.php'); ?>
<!-- SCRIPTS -->

</body>
</html>
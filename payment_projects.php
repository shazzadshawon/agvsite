<div class="wrapper">
    <ul class="button-group filter">
        <li></li>
        <li data-filter="*"><a href=""> show all</a></li>
        <li data-filter=".branding"><a href=""> Branding</a></li>
        <li data-filter=".web"><a href=""> Web</a></li>
        <li data-filter=".photography"><a href=""> Photography</a></li>
    </ul>
    <div id="portfolio-grid" class="isotope-grid isotope-spaced-mini portfolio-container style-modern clearfix" data-heightratio="1">

        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="payment_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/seasons-wallpaper/Story%20of%20Seasons%20Wallpaper3.PNG" alt="Story of Seasons Wallpaper">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">E-Taka</h4>
                    </div>
                </a>
            </div>
        </div>
        <div class="isotope-item portfolio-item branding">
            <div class="portfolio-media">
                <a href="payment_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/hijri/hijri.jpg" alt="Hijri Calendar">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Hijri Calendar</h4>
                    </div>
                </a>
            </div>
        </div>

        <div class="isotope-item portfolio-item photography">
            <div class="portfolio-media">
                <a href="payment_single_page.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                    <img src="files/uploads/services/mobile/chat_offline/Chat%20Offline1.PNG" alt="Chat Offline">
                    <div class="overlay-caption caption-top portfolio_overlay_background_black portfolio_overlay-top">
                        <h6 class="caption-sub portfolio-category subtitle-2">Asain Global Venture (BD) Ltd</h6>
                        <hr class="zigzag">
                        <h4 class="caption-name portfolio-name uppercase">Chat Offline</h4>
                    </div>
                </a>
            </div>
        </div>

    </div> <!-- END #portfolio-grid .isotope-grid -->

</div>
<!doctype html>
<html class="no-js" lang="en-US">
<head>

    <!-- DOCUMENT TITLE -->
    <title>Services | AGVCORP </title>
    <?php require('head.php'); ?>

</head>

<body>

<!-- PAGELOADER -->
<?php require('page_loader.php'); ?>
<!-- PAGELOADER -->

<!-- PAGE CONTENT -->
<div id="page-content">

    <!-- HEADER -->
    <?php require('header.php'); ?>
    <!-- HEADER -->

    <!-- HERO  -->
    <section id="hero" class="hero-auto parallax-section text-light" data-parallax-image="files/uploads/title/services.png">

        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">Services</h4>
            <h1><strong>We Provide Best Solutions</strong></h1>
        </div> <!-- END #page-title -->

    </section>
    <!-- HERO -->


    <!-- PAGEBODY -->
    <section id="page-body">

        <div class="wrapper">
            <div id="portfolio-grid" class="isotope-grid portfolio-container style-modern clearfix" data-heightratio="1">

                <?php require('service_page_grits.php'); ?>


                <div class="isotope-item portfolio-item wide branding">
                    <div class="portfolio-media">
                        <a href="mobile-application.php" class="thumb-overlay overlay-effect-1">
                            <img src="files/uploads/iconImage/Mobile_app.jpeg" alt="Mobile Application">
                            <div class="overlay-caption caption-top">
                                <h6 class="caption-sub portfolio-category subtitle-2">Mobile Application</h6>
                                <hr class="zigzag">
                                <h4 class="caption-name portfolio-name uppercase">Mobile Application</h4>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="isotope-item portfolio-item wide branding">
                    <div class="portfolio-media">
                        <a href="web-application.php" class="thumb-overlay overlay-effect-1 text-light">
                            <img src="files/uploads/iconImage/Web_app.jpg" alt="Web Design & Application">
                            <div class="overlay-caption">
                                <h6 class="caption-sub portfolio-category subtitle-2">Web Design & Application</h6>
                                <hr class="zigzag">
                                <h4 class="caption-name portfolio-name uppercase">Web Design & Application</h4>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div> <!-- END .wrapper -->

        <div class="spacer-big"></div>
        <!--
        <div class="fullwidth-section text-light videobg-section" data-videotype="html5" data-videomp4="files/uploads/stars.mp4" data-videoratio="16/9" data-videoloop="true" data-videomute="true" data-videoposter="files/uploads/stars.jpg" data-videooverlaycolor="#000000" data-videooverlayopacity="0.4">
            <div class="fullwidth-content wrapper align-center">

                <h2><strong>Beautifull Video Backgrounds</strong></h2>
                <h5 class="subtitle-1">Ultra Performance Wordpress theme for creative agencies.<br>We provide perfect and professional services, covering all areas of the modern communication.</h5>
                <div class="spacer-medium"></div>
                <a class="sr-button small-button button-4 rounded" href="http://www.youtube.com/embed/6v2L2UGZJAM" data-rel="lightcase">See out Showreel</a>
            </div>
        </div> -->
        <!-- END .fullwidth-section -->

        <div class="wrapper align-center">
            <img src="files/uploads/logo-sudo-scroll.png" srcset="files/uploads/logo-sudo-scroll.jpg 1x, files/uploads/logo-sudo-scroll@2x.jpg 2x" alt="Logo">
            <h3><strong>Join our Newsletter</strong></h3>
            <h5 class="subtitle-1">Get the latest news from AGV, and be up to date.</h5>
            <div class="spacer-mini"></div>
            <form action="#" method="post" id="newsletter-widget-form">
                <input type="email" id="newsletter-email" name="newsletter-email" placeholder="Your Email Adress">
                <input type="submit" id="newsletter-submit" value="Sign Up">
            </form>
        </div> <!-- END .wrapper -->

        <div class="spacer-big"></div>
        <?php include "click_to.php" ?>
    </section>
    <!-- PAGEBODY -->


    <!-- FOOTER -->
    <?php include "footer.php" ?>
    <!-- FOOTER -->

</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->

<?php require('foot.php'); ?>

</body>
</html>
<?php error_reporting(0); ?>
<?php
$mail_flag = 2;

if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) ){
    if ( !empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message']) ){
        $user_name = $_POST['name'];
        $user_email = $_POST['email'];
        $user_message = $_POST['message'];
        $contact_addr = 'sofwaresolutions@agvcorp.biz';
//        $contact_addr = 'contact@kababiabd.com';

        $subject = 'Contact Page Mail from '.$user_email;
//        $message = $user_message.'<br>'.'regards'.'<br>'.$user_name;
        $message_body = "This is a mail from AGV website Contact-us page. <hr>".
                        "<br/>".
                        "<br/>".
                        $user_message.
                        "<br>".
                        "<hr>".
                        "Name: ".$user_name."<br>".
                        "Email: ".$user_email."<br/>";

        $message = '
                    <style>
                        body {
                            padding: 0;
                            margin: 0;
                        }
                    
                        html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
                        @media only screen and (max-device-width: 680px), only screen and (max-width: 680px) {
                            *[class="table_width_100"] {
                                width: 96% !important;
                            }
                            *[class="border-right_mob"] {
                                border-right: 1px solid #dddddd;
                            }
                            *[class="mob_100"] {
                                width: 100% !important;
                            }
                            *[class="mob_center"] {
                                text-align: center !important;
                            }
                            *[class="mob_center_bl"] {
                                float: none !important;
                                display: block !important;
                                margin: 0px auto;
                            }
                            .iage_footer a {
                                text-decoration: none;
                                color: #929ca8;
                            }
                            img.mob_display_none {
                                width: 0px !important;
                                height: 0px !important;
                                display: none !important;
                            }
                            img.mob_width_50 {
                                width: 40% !important;
                                height: auto !important;
                            }
                        }
                        .table_width_100 {
                            width: 680px;
                        }
                    </style>
                    <div id="mailsub" class="notification" align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">
                            <table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
                                <tr><td>                
                                </td></tr>
                                <tr><td align="center" bgcolor="#ffffff">
                                    <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                        <tr><td align="center">
                                            <a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; float:left; width:100%; padding:20px;text-align:center; font-size: 13px;">
                                                <font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#596167">
                                                    <img src="http://www.basis.org.bd/logo/a3ff7c89595fa36153d8ae1c8ab071a1.png" width="250" alt="Metronic" border="0"  /></font></a>
                                        </td>
                                            <td align="right">
                                            </td></tr>
                                        <tr><td align="center" bgcolor="#fbfcfd">
                                            <font face="Arial, Helvetica, sans-serif" size="4" color="#57697e" style="font-size: 15px;">
                                                <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr><td>
                                                        '.$message_body.'
                                                    </td></tr>
                                                </table>
                                            </font>
                                        </td></tr>
                                        <tr><td class="iage_footer" align="center" bgcolor="#ffffff">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr><td align="center" style="padding:20px;flaot:left;width:100%; text-align:center;">
                                                    <font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
                                        2017 - 2020 © AGV. ALL Rights Reserved.
                                    </span></font>
                                                </td></tr>
                                            </table>
                                        </td></tr>
                                        <tr><td>
                                        </td></tr>
                                    </table>
                                </td></tr>
                            </table>';

        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        $headers .= $user_email. "\r\n";

        if ( mail($contact_addr, $subject, $message, $headers) ){
            $mail_flag = 1;
        } else {
            $mail_flag = 0;
        }
    }
}
?>

<!doctype html>
<html class="no-js" lang="en-US">
<head>

    <!-- DOCUMENT TITLE -->
    <title>Contact | AGVCORP </title>
    <?php require('head.php'); ?>

</head>

<body>

<!-- PAGELOADER -->
<?php require('page_loader.php'); ?>
<!-- PAGELOADER -->

<!-- PAGE CONTENT -->
<div id="page-content">

    <!-- HEADER -->
    <?php require('header.php'); ?>
    <!-- HEADER -->

    <!-- HERO  -->
    <section id="hero" class="hero-full parallax-section" data-parallax-image="files/uploads/title/pexels-photo-large.jpg">

        <div id="page-title" class="wrapper align-center">
            <h4 class="subtitle-2">Contact</h4>
            <hr class="zigzag mini colored">
            <h1><strong>How can we help you</strong></h1>
        </div> <!-- END #page-title -->

    </section>
    <!-- HERO -->


    <!-- PAGEBODY -->
    <section id="page-body" class="notoppadding">

        <div class="column-section boxed-sticky adapt-height vertical-center clearfix">
            <div class="column one-third align-center text-light has-animation" style="background:#1a1a1a;">
                <div class="spacer-medium"></div>
                <h5 class="uppercase">Thailand Office</h5>
                <hr class="zigzag mini">
                <p>173 Elizabeth Street<br>67000 Sydney </p>
                <div class="spacer-medium"></div>
            </div>
            <div class="column one-third align-center text-light has-animation" data-delay="100" style="background:#000000;">
                <div class="spacer-medium"></div>
                <h5 class="uppercase">Phone</h5>
                <hr class="zigzag mini">
                <p>+12 212-568-999+12<br>323-999-568</p>
                <div class="spacer-medium"></div>
            </div>
            <div class="column one-third last-col align-center text-light has-animation" data-delay="200" style="background:#1a1a1a;">
                <div class="spacer-medium"></div>
                <h5 class="uppercase">Email</h5>
                <hr class="zigzag mini">
                <p>sayhello@sudo.com<br>appointment@sudo.com</p>
                <div class="spacer-medium"></div>
            </div>
        </div> <!-- END .column-section -->

        <div class="column-section boxed-sticky adapt-height vertical-center clearfix">
            <div class="column one-third align-center text-light has-animation" style="background:#000000;">
                <div class="spacer-medium"></div>
                <h5 class="uppercase">Bangladesh Office</h5>
                <hr class="zigzag mini">
                <p>House-3,Road-28,Block-K<br>Banani, Dhaka-1213 </p>
                <div class="spacer-medium"></div>
            </div>
            <div class="column one-third align-center text-light has-animation" data-delay="100" style="background:#1a1a1a;">
                <div class="spacer-medium"></div>
                <h5 class="uppercase">Phone</h5>
                <hr class="zigzag mini">
                <p>+88029850613<br>+88029850614</p>
                <div class="spacer-medium"></div>
            </div>
            <div class="column one-third last-col align-center text-light has-animation" data-delay="200" style="background:#000000;">
                <div class="spacer-medium"></div>
                <h5 class="uppercase">Email</h5>
                <hr class="zigzag mini">
                <p>info@agvcorp.biz<br>sofwaresolutions@agvcorp.biz</p>
                <div class="spacer-medium"></div>
            </div>
        </div> <!-- END .column-section -->

        <div class="spacer-big"></div>

        <div class="wrapper-small">

            <div class="align-center">
                <hr class="zigzag medium">
                <div class="spacer-mini"></div>
                <h5 class="subtitle-2 colored">Get Social</h5>
                <ul class="socialmedia-widget style-circled size-small hover-slide-1">
                    <li class="facebook"><a href="https://www.facebook.com/agvcorp/?fref=ts)"></a></li>
                    <li class="twitter"><a href="https://twitter.com/agvcorp"></a></li>
                    <li class="googleplus"><a href="https://plus.google.com/112250042386612154392/about"></a></li>
                </ul>
                <div class="spacer-mini"></div>
                <hr class="zigzag medium">
            </div>

            <div class="spacer-medium"></div>

            <h3 class="align-center"><strong>Got a Question?</strong></h3>
            <h5 class="subtitle-2 align-center">Have a project you're interested in discussing with us?<br> Drop us a line below, we'd love to talk.</h5>


            <div class="spacer-medium"></div>
            <form id="" class="" action="contact-us.php" method="post">
                <div class="form-row">
                    <label for="name">Name <abbr title="required" class="required">*</abbr></label>
                    <input type="text" name="name" id="name" class="name req" value="">
                </div>

                <div class="form-row">
                    <label for="email">Email <abbr title="required" class="required">*</abbr></label>
                    <input type="text" name="email" id="email" class="email req" value="">
                </div>

                <div class="form-row">
                    <label for="message">Message <abbr title="required" class="required">*</abbr></label>
                    <textarea name="message" id="message" class="message req" rows="15" cols="50"></textarea>
                </div>

                <?php
                if ($mail_flag == 1){
                    echo '<div class="form-row form-note"> <div class="alert-error"> <h5>Success!</h5> You mail has successfully sent to sofwaresolutions@agvcorp.biz!</div> </div>';
                } else if ($mail_flag == 2){
                    echo 'Send Message';
                } else if ($mail_flag == 0){
                    echo '<div class="form-row form-note"> <div class="alert-error"> <h5>Something Went Wrong!</h5> Server Overloaded. Please try again after some time!</div> </div>';
                }
                ?>


                <div class="form-row hidden">
                    <input type="text" id="form-check" name="form-check" value="" class="form-check">
                </div> <!-- Spam check field -->

                <div class="form-row">
                    <input type="submit" name="submit" class="submit" value="Send Message">
                </div>
            </form>

        </div> <!-- END .wrapper-small -->

        <div class="spacer-big"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6"><div id="map1" class="google-map " style="height:400px;"></div></div>
                <div class="col-md-6"><div id="map" class="google-map " style="height:400px;"></div></div>
            </div>
        </div>
    </section>
    <!-- PAGEBODY -->


    <!-- FOOTER -->
    <?php include "footer.php" ?>
    <!-- FOOTER -->

</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->

<?php require('foot.php'); ?>
<script type="text/javascript" src="files/js/form-validation.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCH9wv1AisIK69XKRoHqq8dUUR1jgh69Rw"></script>

<script type="text/javascript">
    function mapinitialize1() {
        var ban = new google.maps.LatLng(23.794422, 90.409057);
        var thai = new google.maps.LatLng(13.7562613, 100.5329366);
        var banoption = {
            zoom: 15,
            center: ban,
            scrollwheel: false,
            scaleControl: false,
            disableDefaultUI: false,
            styles: [{featureType:"landscape",stylers:[{saturation:-100},{lightness:65},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"simplified"}]},{featureType:"road.highway",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},{visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}]},{featureType:"water",elementType:"labels",stylers:[{visibility:"on"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{hue:"#ffff00"},{lightness:-25},{saturation:-97}]}],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var thaiOptions = {
            zoom: 14,
            center: thai,
            scrollwheel: false,
            scaleControl: false,
            disableDefaultUI: false,
            styles: [{"elementType":"geometry","stylers":[{"color":"#242f3e"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#746855"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#242f3e"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#d59563"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#d59563"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#263c3f"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#6b9a76"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#38414e"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#212a37"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#9ca5b3"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#746855"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#1f2835"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#f3d19c"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#2f3948"}]},{"featureType":"transit.station","elementType":"labels.text.fill","stylers":[{"color":"#d59563"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#17263c"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#515c6d"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#17263c"}]}],
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map1"),banoption);
        var map1 = new google.maps.Map(document.getElementById("map"),thaiOptions);

        var image = new google.maps.MarkerImage("files/uploads/map_pin.svg", null, null, null, new google.maps.Size(51,68));
        var marker = new google.maps.Marker({
            map: map,
            icon: image,
            position: map.getCenter()
        });

        var marker1 = new google.maps.Marker({
            map: map1,
            icon: image,
            position: map1.getCenter()
        });

        var bancontentString = "Asian Globan Ventures (BD) Ltd. H- R-28, B-K, 1213, Rd No 3, Dhaka 1213 / We don't offer a typical set of programs. We don't do the same boring plot day-after-day. Everything we do is customized for our clients.";
        var thaicontentString = "Asian Global Ventures (Thailand) Company Limited, 128/137, 13th floor, Payathai Plaza, Payathai Road,, Payathai, Ratchathewee, Bangkok 10400 / We are an on stop IT solution provider and we provide everything to successfully market your business to customer.";
        var baninfowindow = new google.maps.InfoWindow({
            content: bancontentString
        });
        var thaiinfowindow = new google.maps.InfoWindow({
            content: thaicontentString
        });

        google.maps.event.addListener(marker, "click", function() {
            baninfowindow.open(map,marker);
        });
        google.maps.event.addListener(marker1, "click", function() {
            thaiinfowindow.open(map1,marker1);
        });


    }
    mapinitialize1();
</script>

</body>
</html>
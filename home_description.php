<div class="wrapper">

    <h3 class="align-center"><strong>We provide perfect & professional service</strong></h3>
    <h5 class="subtitle-1 align-center">That saves money and time</h5>
    <div class="spacer-big"></div>

    <div class="column-section clearfix">
        <div class="column one-half">
            <div class="icon-box">
                <div class="icon-box-icon"><i class="ion ion-4x ion-ios-speedometer-outline colored"></i></div>
                <div class="icon-box-content">
                    <h5 class="uppercase">STRENGTHS</h5>
                    <ul>
                        <li>One Stop Resource Platform for IT solution from A to Z</li>
                        <li>Dedicated professional team with web solutions, business process automation and custom software development expertise </li>
                        <li>Comprehensive experience of successfully delivering customized e-commerce and business solution with 100% client satisfaction</li>
                        <li>Vast knowledge on budget assessment and implication </li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="column one-half last-col">
            <div class="icon-box">
                <div class="icon-box-icon"><i class="ion ion-4x ion-ios-settings colored"></i></div>
                <div class="icon-box-content">
                    <h5 class="uppercase">SPECIALIZATION</h5>
                    <ul>
                        <li>Work on high-end technologies</li>
                        <li>Work on large volume and delicate projects. </li>
                        <li>Implementation of finish quality practices and procedure</li>
                        <li>System programming</li>
                        <li>Custom website design, website development, database management. </li>
                        <li>Custom mobile application, web application & desktop application development</li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- END .column-section -->
    <div class="column-section clearfix">
        <div class="column one-half">
            <div class="icon-box">
                <div class="icon-box-icon"><i class="ion ion-4x ion-ios-color-wand-outline colored"></i></div>
                <div class="icon-box-content">
                    <h5 class="uppercase">Team Work</h5>
                    <ul>
                        <li>Strong leadership</li>
                        <li>Strategy sharing</li>
                        <li>Proper work distribution</li>
                        <li>Clear communication</li>
                        <li>Common objective & goal</li>
                        <li>Good at heart</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="column one-half last-col">
            <div class="icon-box">
                <div class="icon-box-icon"><i class="ion ion-4x ion-ios-book-outline colored"></i></div>
                <div class="icon-box-content">
                    <h5 class="uppercase">Well Documented</h5>
                    <ul>
                        <li>ISO 9001:2008 certified. </li>
                        <li>ISO 27001:2013 certified</li>
                        <li>Member of BASIS: Membership No. G481</li>
                        <li>Member of BCS: Membership ID: 1330</li>
                        <li>Enlisted to Bangladesh navy </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- END .column-section -->
</div> <!-- END .wrapper -->
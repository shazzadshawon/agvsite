module.exports = function(grunt){

    // configure Main project setting
    grunt.initConfig({

        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7,
                    svgoPlugins: [{removeViewBox: false}]
                },
                files: [
                    {
                        expand: true,
                        cwd: 'files/uploads',
                        src: ['**/*.{png,jpg,gif}'],
                        dest: 'files/compressed/'
                    }
                ]
            }
        },

        // Basic Settings and info about plugins
        pkg: grunt.file.readJSON('package.json'),

        // Name of Plugins
        cssmin: {
            combine: {
                files: {
                    'files/css/allStyles.min.css': ['files/css/style.css',
                        'files/css/owl.carousel.css',
                        'files/css/lightcase.css',
                        'files/css/isotope.css',
                        'files/css/mqueries.css',
                        'files/revolution/css/settings.css',
                        'files/revolution/css/layers.css',
                        'files/revolution/css/navigation.css']
                    // 'files/revolution/css/revo.min.css': [
                    //     'files/revolution/css/settings.css',
                    //     'files/revolution/css/layers.css',
                    //     'files/revolution/css/navigation.css']
                }
            }
        },

        uglify: {
            dist1: {
                files: {
                    'files/js/agv.js': ['files/js/jquery.easing.1.3.js',
                        'files/js/jquery.visible.min.js',
                        'files/js/tweenMax.js',
                        'files/js/jquery.backgroundparallax.min.js',
                        'files/js/jquery.isotope.min.js',
                        'files/js/packery.min.js',
                        'files/js/jquery.imagesloaded.min.js',
                        'files/js/jquery.owl.carousel.js',
                        'files/js/jquery.lightcase.min.js']
                }
            },
            dist2: {
                files: {
                    'files/js/customscript.min.js': ['files/js/script.js']
                }
            },
            dist3: {
                files: {
                    'files/js/revolution.min.js': ['files/revolution/js/jquery.themepunch.tools.min.js?rev=5.0',
                        'files/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0',
                        'files/revolution/js/extensions/revolution.extension.slideanims.min.js',
                        'files/revolution/js/extensions/revolution.extension.layeranimation.min.js',
                        'files/revolution/js/extensions/revolution.extension.navigation.min.js',
                        'files/revolution/js/extensions/revolution.extension.video.min.js',
                        'files/revolution/js/extensions/revolution.extension.actions.min.js']
                }
            }
        }
    });

    // Load the Plugin
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Exicute The Task
    // grunt.registerTask('default', ['cssmin', 'uglify:dist1', 'uglify:dist2', 'uglify:dist3', 'imagemin']);
    grunt.registerTask('default', ['cssmin']);
};
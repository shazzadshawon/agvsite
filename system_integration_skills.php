<div class="column-section clearfix">
    <div class="column one-third align-right">
        <h3><strong>Our Skills</strong></h3>
        <h5 class="subtitle-2">See what we're able to do.</h5>
    </div>
    <div class="column two-third last-col">
        <div class="progress-bar-item">
            <h6 class="progress-name">Networking/h6>
                <div class="progress-bar"><div class="progress-active" style="background: #ff4848" data-perc="95"><span class="tooltip">95%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Enbeded Technologies</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #a02727" data-perc="70"><span class="tooltip">70%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Robotics</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #ffb1b1" data-perc="40"><span class="tooltip">40%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Agumented Reality</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #73b6e6" data-perc="45"><span class="tooltip">45%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">VR</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #185a8a" data-perc="80"><span class="tooltip">80%</span></div></div>
        </div>
    </div>
</div>
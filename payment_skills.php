<div class="column-section clearfix">
    <div class="column one-third align-right">
        <h3><strong>Our Skills</strong></h3>
        <h5 class="subtitle-2">See what we're able to do.</h5>
    </div>
    <div class="column two-third last-col">
        <div class="progress-bar-item">
            <h6 class="progress-name">Java/h6>
                <div class="progress-bar"><div class="progress-active" style="background: #ff4848" data-perc="90"><span class="tooltip">90%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">C#</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #a02727" data-perc="80"><span class="tooltip">80%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">C</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #ffb1b1" data-perc="50"><span class="tooltip">50%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Objective C</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #73b6e6" data-perc="75"><span class="tooltip">75%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">.Net</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #185a8a" data-perc="70"><span class="tooltip">70%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Python</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
    </div>
</div>
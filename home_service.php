<div class="wrapper">

    <h3 class="align-center"><strong>We Provide Customized Solution</strong></h3>
    <div class="spacer-big"></div>


    <div id="portfolio-grid" class="lazy isotope-grid isotope-spaced-mini portfolio-container style-modern clearfix" data-heightratio="1">

    <?php require('home_service_grits.php'); ?>
    <div class="isotope-item portfolio-item tall branding">
        <div class="portfolio-media">
            <a href="mobile-application.php" class="thumb-overlay overlay-effect-1" target="">
                <img class="lazy portfolio_image_opacity" data-src="files/uploads/iconImage/Mobile_app.jpeg" alt="Mobile Application">
                <div class="overlay-caption caption-top portfolio_overlay_background portfolio_overlay-top">
                    <h6 class="caption-sub portfolio-category subtitle-2">Mobile Application</h6>
                    <hr class="zigzag">
                    <h4 class="caption-name portfolio-name uppercase portfolio_caption">Mobile Application</h4>
                </div>
            </a>
        </div>
    </div>
    <div class="isotope-item portfolio-item branding">
        <div class="portfolio-media">
            <a href="desktop-application.php" class="thumb-overlay overlay-effect-1 text-light" target="">
                <img class="portfolio_image_opacity lazy" data-src="files/uploads/iconImage/Desktop_app.jpeg" alt="Desktop Application">
                <div class="overlay-caption portfolio_overlay-bottom portfolio_overlay_background">
                    <h6 class="caption-sub portfolio-category subtitle-2">Desktop Application</h6>
                    <hr class="zigzag">
                    <h4 class="caption-name portfolio-name uppercase portfolio_caption">Desktop Application</h4>
                </div>
            </a>
        </div>
    </div>

    </div>  <!-- END #portfolio-grid .isotope-grid -->

</div> <!-- END .wrapper -->
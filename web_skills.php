<div class="column-section clearfix">
    <div class="column one-third align-right">
        <h3><strong>Our Skills</strong></h3>
        <h5 class="subtitle-2">See what we're able to do.</h5>
    </div>
    <div class="column two-third last-col">
        <div class="progress-bar-item">
            <h6 class="progress-name">HTML/HTML5h6>
                <div class="progress-bar"><div class="progress-active" style="background: #ff4848" data-perc="90"><span class="tooltip">90%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">CSS/SASS/LESS</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #a02727" data-perc="80"><span class="tooltip">80%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">jQuery</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #ffb1b1" data-perc="50"><span class="tooltip">50%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">JavaScript</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #73b6e6" data-perc="75"><span class="tooltip">75%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">PHP</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #185a8a" data-perc="70"><span class="tooltip">70%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Laravel</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">MySQl</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">Bootstrap</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">NodeJS</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">AngularJS</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
        <div class="progress-bar-item">
            <h6 class="progress-name">ReactJS</h6>
            <div class="progress-bar"><div class="progress-active" style="background: #005b9c" data-perc="60"><span class="tooltip">60%</span></div></div>
        </div>
    </div>
</div>